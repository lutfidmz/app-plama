# PLAMA - Play Mate

#### Never play alone anymore

Apps that gather venues & players around the world.
You can either host your event and join as a player.

[Website](http://plama.herokuapp.com/ "Website")
[Documentation & How To Use](http://http://plama.herokuapp.com/docs/index.html "Documentation")

Author : [Lutfidmz](https://www.linkedin.com/in/lutfidmz/ "Lutfidmz")
Website : [lutfidmz.tech](http://lutfidmz.tech "lutfidmz.tech")
