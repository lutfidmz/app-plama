import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Bookings extends BaseSchema {
  protected tableName = 'bookings'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary();
      table.integer('required_player'),
        table.datetime('play_date_start', { useTz: true })
      table.datetime('play_date_end', { useTz: true })
      table.uuid('user_id').references('id').inTable('users').onDelete('CASCADE')
      table.uuid('field_id').references('id').inTable('fields').onDelete('CASCADE')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
