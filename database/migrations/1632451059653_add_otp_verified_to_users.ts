import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Users extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('otp_code').defaultTo(null).after('role')
      table.boolean('is_verified').defaultTo(null).after('role')
    })
  }

  public async down () {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumns('otp_code', 'is_verified')
    })
  }
}
