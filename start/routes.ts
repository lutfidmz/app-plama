/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
Route.get('/', async ({ response }) => {
  return response.redirect('docs')
})

Route.group(() => {

  Route.post('/login', 'UsersController.login').as('users.login').middleware('verified')
  Route.post('/register', 'UsersController.register').as('users.register')
  Route.post('/otp-confirmation', 'UsersController.otpConfirmation').as('users.otpConfirmation')


  Route.group(() => {
    Route.resource('/venues', 'VenuesController').apiOnly().as('venues').middleware({
      'index': ['role:user,admin,owner'],
      'show': ['role:user,admin,owner'],
      'store': ['role:admin,owner'],
      'update': ['role:admin,owner'],
      'destroy': ['role:admin,owner'],
    })

    Route.group(() => {
      Route.resource('venues.fields', 'FieldsController').apiOnly().as('venues.fields')
      Route.post('/venues/:id/bookings', 'BookingsController.store').as('bookings.store')

      Route.get('/bookings', 'BookingsController.index').as('bookings.index')
      Route.get('/bookings/:id', 'BookingsController.show').as('bookings.show')
      Route.put('/bookings/:id/join', 'BookingsController.join').as('bookings.join')
      Route.put('/bookings/:id/unjoin', 'BookingsController.unjoin').as('bookings.unjoin')
      Route.get('/schedules', 'BookingsController.schedule').as('bookings.schedule')
    }).middleware('role:user,admin,owner')

  }).middleware(['auth'])
}).prefix('/api/v1')


