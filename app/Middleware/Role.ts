import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Role {
  public async handle({ auth, response }: HttpContextContract, next: () => Promise<void>, allowedRole: string[]) {
    const allow = allowedRole.find(role => role == auth.user!.role)

    if (allow) {
      await next()
    } else {
      response.badRequest({ message: `Role ${auth.user!.role} is not allowed, Only ${allowedRole.join(" - ")} allowed!!` })
    }

  }
}
