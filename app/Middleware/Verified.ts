import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User';

export default class Verified {
  public async handle({ request, response, auth }: HttpContextContract, next: () => Promise<void>) {
    if (auth.user?.isVerified) {
      await next()
    } else {
      const user = await User.findByOrFail("email", request.body().email)
      if (user.isVerified) {
        await next()
        return
      }
      response.unauthorized({ message: 'akun anda belum terverifikasi!', user })
    }
  }
}
