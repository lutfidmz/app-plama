import Mail from '@ioc:Adonis/Addons/Mail'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import CreateUserValidator from 'App/Validators/CreateUserValidator'
import Env from '@ioc:Adonis/Core/Env'


export default class UsersController {
    public async register({ request, response }: HttpContextContract) {
        try {
            // const { name, email, password, role } = request.all()
            const validateUser = await request.validate(CreateUserValidator)
            const otp_code = Math.floor(676777 * Math.random() * 900)
            const user = new User()

            user.fill({ ...validateUser, otp_code })
            await user.save()

            await Mail.send((message) => {
                message
                    .from(Env.get('SMTP_USERNAME'))
                    .to(validateUser.email)
                    .subject('Welcome Onboard!')
                    .htmlView('emails/otp_confirmation', { name: validateUser.name, otp_code })
            })

            response.created({ message: "Registered!, Cek Email untuk verifikasi OTP", data: user })

        } catch (error) {
            response.badRequest({ message: error.message })
        }
    }

    public async login({ request, response, auth }: HttpContextContract) {
        let { email, password } = request.all()

        const token = await auth.use('api').attempt(email, password)
        response.ok({ message: "Berhasil login", token })
    }

    public async otpConfirmation({ request, response }: HttpContextContract) {
        try {
            const createSchema = schema.create({
                email: schema.string({ trim: true }),
                otp_code: schema.number(),
            })

            const { email, otp_code } = await request.validate({ schema: createSchema })

            console.log("ini ni", otp_code, email);

            let user = await User.query().where("otp_code", otp_code).where("email", email).firstOrFail()

            await user.merge({
                isVerified: true,
                otp_code: null
            }).save()

            response.ok({ message: "Berhasil konfirmasi OTP" })

        } catch (error) {
            response.badRequest({ message: error.message })
        }
    }
}
