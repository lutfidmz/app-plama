import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import User from 'App/Models/User'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'
import { uuid } from 'uuidv4'

export default class BookingsController {
  public async index({ response }: HttpContextContract) {
    const booking = await Booking
      .query()
      .select()
      .withCount('player', player => player.as('joined_player'))
      .preload('field', field => {
        field.preload('venue', venue => venue.select('name', 'address', 'phone'))
        field.select('name', 'type', 'venue_id')
      })

    response.ok({ message: 'Berhasil mengambil data booking', data: booking })
  }

  public async show({ response, params }: HttpContextContract) {
    try {
      const booking = await Booking
        .query()
        .where('id', params.id)
        .withCount('player', player => player.as('joined_player'))
        .preload('player', player => player.preload('user', user => user.select('name')))
        .preload('field', field => {
          field.preload('venue', venue => venue.select('name', 'address', 'phone'))
          field.select('name', 'type', 'venue_id')
        })
        .firstOrFail()

      response.ok({ message: 'Berhasil mengambil data detail booking', data: booking })

    } catch (error) {
      response.badRequest(error)
    }
  }




  public async store({ auth, response, request }: HttpContextContract) {
    let bookingValidator = await request.validate(CreateBookingValidator)
    let payload = { ...bookingValidator }
    const field = await Field.findOrFail(payload.field_id)


    const booking = new Booking()
    booking.requiredPlayer = bookingValidator.required_player
    // booking.playDateStart = bookingValidator.play_date_start.toJSDate()
    // booking.playDateEnd = bookingValidator.play_date_end.toJSDate()
    booking.playDateEnd = bookingValidator.play_date_end
    booking.userId = auth.user!.id

    await field.related('booking').save(booking)

    response.created({ message: 'Berhasil membuat booking', data: booking })
  }

  public async schedule({ auth, response }: HttpContextContract) {
    try {
      const data = await User.query()
        .where('id', auth.user!.id)
        .preload('booking', booking => {
          booking.preload('field', field => {
            field.select('name', 'type', 'venue_id')
            field.preload('venue', venue => venue.select('name', 'address', 'phone'))
          })
        })

      // const data = await Player.query().where('user_id', auth.user!.id).preload('booking')

      response.ok({ message: 'Berhasil mendapatkan data schedule', data })
    } catch (error) {
      response.badRequest(error.message)
    }
  }

  public async join({ auth, response, params }: HttpContextContract) {
    try {
      const booking = await Booking.findOrFail(params.id)
      const user = await User.findOrFail(auth.user!.id)

      const id = uuid()

      const user_booking = await user.related('booking').attach({
        [booking.id]: { id }
      })

      response.ok({ message: 'Berhasil Join', data: user_booking })
    } catch (error) {
      response.badRequest(error)
    }
  }

  public async unjoin({ auth, response, params }: HttpContextContract) {
    try {
      const booking = await Booking.findOrFail(params.id)
      const user = await User.findOrFail(auth.user!.id)

      const user_booking = await user.related('booking').detach([booking.id])

      response.ok({ message: 'Berhasil Un-Join', data: user_booking })
    } catch (error) {
      response.badRequest(error)
    }
  }
}
