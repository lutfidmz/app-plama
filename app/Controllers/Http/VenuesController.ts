import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Venue from 'App/Models/Venue'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import { schema } from '@ioc:Adonis/Core/Validator'



export default class VenuesController {
  public async index({ response }: HttpContextContract) {
    try {
      const venue = await Venue.query().preload('field')

      response.ok({ message: 'Berhasil mengambil data venue', data: venue })

    } catch (error) {
      response.badRequest(error.message)
    }

  }

  public async store({ auth, request, response }: HttpContextContract) {
    const newVenue = await request.validate(CreateVenueValidator)
    const user = auth.user!

    const venue = new Venue()
    venue.merge(newVenue)

    await user.related('venue').save(venue)

    response.created({ message: 'Berhasil menyimpan venue', venue })
  }

  public async show({ response, params, request }: HttpContextContract) {
    const dateSchema = schema.create({
      date_filter: schema.date.optional({ format: 'yyyy-MM-dd' })
    })

    const payload = await request.validate({ schema: dateSchema })
    try {
      console.log('masuk atas');

      let venue: object
      if (payload.date_filter) {
        console.log('masuk awal');

        venue = await Venue
          .query()
          .where('id', params.id)
          .preload('booking', (bookingQuery) => {
            // FIXME: error Tz
            // bookingQuery.whereRaw('DATEDIFF(play_date_start, :date_filter) = 0', { date_filter: payload.date_filter!.toSQLDate() })
            bookingQuery.preload('field', fieldQuery => {
              fieldQuery.select('name', 'type')
            });
          })
          .firstOrFail()

      } else {
        console.log('masuk akhir');

        venue = await Venue
          .query()
          .where('id', params.id)
          .preload('booking', () => {
            // .preload('booking', (bookingQuery) => {
            // bookingQuery.whereRaw('DATEDIFF(play_date_start, now()) = 0')
            // FIXME: error karena ada Tz nya
          })
          .firstOrFail()
      }

      response.ok({ message: 'Berhasil mendapatkan data venue', data: venue })

    } catch (error) {
      response.badRequest(error.message)
    }

  }

  public async update({ request, response, params, auth }: HttpContextContract) {
    try {
      const newVenue = await request.validate(CreateVenueValidator)
      const venue = await Venue.findOrFail(params.id)

      // Cek pemilik venue
      if (auth.user!.id != venue.userId) {
        response.badRequest({ message: "Anda bukan pemilik venue ini" })
        return
      }

      await venue.merge(newVenue).save()
      response.ok({ message: "Berhasil edit data venue", data: venue })

    } catch (error) {
      response.unprocessableEntity(error.message)
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      const venue = await Venue.findOrFail(params.id)

      await venue.delete()
      response.ok({ message: 'Berhasil menghapus venue' })
    } catch (error) {
      response.badRequest(error.message)
    }
  }
}
