import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'

export default class FieldsController {
  public async index({ response, params }: HttpContextContract) {
    try {
      const venue = await Venue
        .query()
        .where('id', params.venue_id)
        .preload('field')
        .firstOrFail()

      response.ok({ message: 'Berhasil mengambil data fields', data: venue })


    } catch (error) {
      response.badRequest(error)
    }
  }

  public async store({ request, response, params }: HttpContextContract) {
    try {
      const newField = await request.validate(CreateFieldValidator)
      const { venue_id } = params

      const venue = await Venue.findOrFail(venue_id)
      const field = new Field()

      field.fill(newField)
      await venue.related('field').save(field)

      response.created({ message: 'Berhasil menyimpan fields', data: field })

    } catch (error) {
      response.unprocessableEntity(error)
    }
  }

  public async show({ response, params }: HttpContextContract) {
    const { venue_id, id } = params
    const field = await Field.query().where('id', id).where('venue_id', venue_id).preload('venue').firstOrFail()
    response.ok({ message: 'Berhasil mendapatkan data detail field', data: field })
  }

  public async update({ request, response, params }: HttpContextContract) {
    try {
      const newField = await request.validate(CreateFieldValidator)
      const { venue_id, id } = params
      const field = await Field.query().where('venue_id', venue_id).where('id', id).firstOrFail()

      await field.merge(newField).save()

      response.created({ message: 'Berhasil mengubah field', data: field })

    } catch (error) {
      response.unprocessableEntity(error)
    }
  }

  public async destroy({ response, params }: HttpContextContract) {
    try {
      const field = await Field.findOrFail(params.id)
      await field.delete()
      response.ok({ message: 'Berhasil menghapus field' })
    } catch (error) {
      response.badRequest(error.message)
    }
  }

}
