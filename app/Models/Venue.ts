import { DateTime } from "luxon";
import {
  afterCreate,
  BaseModel,
  beforeCreate,
  BelongsTo,
  belongsTo,
  column,
  HasMany,
  hasMany,
  HasManyThrough,
  hasManyThrough,
} from "@ioc:Adonis/Lucid/Orm";
import { uuid } from "uuidv4";
import User from "App/Models/User";
import Field from "App/Models/Field";
import Booking from "./Booking";

let globalId: string

export default class Venue extends BaseModel {

  @column({ isPrimary: true })
  public id: string;

  @column()
  public name: string;

  @column()
  public phone: string;

  @column()
  public address: string;

  @column()
  public userId: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @beforeCreate()
  public static generateUuid(venue: Venue) {
    globalId = uuid()
    venue.id = globalId;
  }

  @afterCreate()
  public static assignUuid(venue: Venue) {
    venue.id = globalId
  }

  @belongsTo(() => User)
  public user: BelongsTo<typeof User>;

  @hasMany(() => Field)
  public field: HasMany<typeof Field>;

  @hasManyThrough([
    () => Booking,
    () => Field,
  ])
  public booking: HasManyThrough<typeof Booking>
}
