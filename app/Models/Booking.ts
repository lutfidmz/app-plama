import { DateTime } from 'luxon'
import { afterCreate, BaseModel, beforeCreate, belongsTo, BelongsTo, column, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import { uuid } from 'uuidv4'
import User from './User'
import Field from './Field'
import Player from './Player'

let globalId: string

export default class Booking extends BaseModel {

  public serializeExtras() {
    return { joined_player: this.$extras.joined_player }
  }

  @column({ isPrimary: true })
  public id: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public requiredPlayer: number

  @column()
  public playDateStart: DateTime

  @column()
  public playDateEnd: DateTime

  @column()
  public userId: string

  @column()
  public fieldId: string

  @beforeCreate()
  public static generateUuid(booking: Booking) {
    globalId = uuid()
    booking.id = globalId;
  }

  @afterCreate()
  public static assignUuid(booking: Booking) {
    booking.id = globalId
  }

  @belongsTo(() => User)
  public user: BelongsTo<typeof User>

  @belongsTo(() => Field)
  public field: BelongsTo<typeof Field>

  @hasMany(() => Player)
  public player: HasMany<typeof Player>

}

