import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  beforeCreate,
  hasMany,
  HasMany,
  afterCreate,
  manyToMany,
  ManyToMany,
} from '@ioc:Adonis/Lucid/Orm'

import { uuid } from 'uuidv4'
import Venue from './Venue'
import Booking from './Booking'

let globalId: string

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public name: string

  @column()
  public role: string

  @column()
  public email: string

  @column()
  public otp_code: number | null

  @column()
  public isVerified: boolean

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  @beforeCreate()
  public static async generateUuid(user: User) {
    globalId = uuid()
    user.id = globalId
  }

  @afterCreate()
  public static assignUuid(user: User) {
    user.id = globalId
  }

  @hasMany(() => Venue)
  public venue: HasMany<typeof Venue>

  @manyToMany(() => Booking, {
    pivotTable: 'players',
    pivotTimestamps: true
  })
  public booking: ManyToMany<typeof Booking>

}
