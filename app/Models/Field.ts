import { DateTime } from 'luxon'
import { afterCreate, BaseModel, beforeCreate, BelongsTo, belongsTo, column, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'
import { uuid } from 'uuidv4'
import Venue from 'App/Models/Venue'
import Booking from 'App/Models/Booking'
let globalId: string

export default class Field extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public name: string

  @column()
  public type: string

  @column()
  public venueId: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeCreate()
  public static generateUuid(field: Field) {
    globalId = uuid()
    field.id = globalId
  }

  @afterCreate()
  public static assignUuid(field: Field) {
    field.id = globalId
  }

  @belongsTo(() => Venue)
  public venue: BelongsTo<typeof Venue>

  @hasMany(() => Booking)
  public booking: HasMany<typeof Booking>
}
